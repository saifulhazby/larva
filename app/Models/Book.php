<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphtoMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Book extends Model
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $fillable = ['book_code', 'title', 'status'];

    // public function user(): HasMany
    // {
    //     return $this->hasMany(User::class, 'user_id', 'id');
    // }

    public function rent(): BelongsTo
    {
       return $this->belongsTo(Rent::class, 'rent_id');
    }

    // public function books_category(): HasMany
    // {
    //     return $this->hasMany(Books_category::class, 'book_id', 'id');
    // }
}
