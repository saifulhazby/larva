<?php

namespace App\Http\Controllers;

use App\Models\Rent_logs;
use App\Http\Resources\RentResource;
use App\Http\Resources\RentDetailResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $Rent_logs = Rent_logs::all();
        //return response()->json(['data' => $data]);
        return RentDetailResource::collection($Rent_logs->loadMissing('user:id,username,nope,address','book:id,title,book_code'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //dd(Auth::user()->id);
        $request->validate([
            //'user_id' => 'required|exists:users,id',
            'book_id' => 'required|exists:books,id',
            'rent_date' => 'required',
            'return_date' => 'required',
            'actual_return_date' => 'required',
        ]);


        $request['user_id'] = Auth()->user()->id;
        $rent = Rent_logs::create($request->all());
        
        return new RentdetailResource($rent->loadMissing('user:id,username,nope,address','book:id,title,book_code'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $Rent_logs = Rent_logs::with('user:id,username,nope,address', 'book:id,book_code,title')->FindOrFail($id);
        //return response()->json(['data' => $data]);
        return new RentDetailResource($Rent_logs);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'book_id' => 'required|exists:books,id',
            'rent_date' => 'required',
            'return_date' => 'required',
            'actual_return_date' => 'required',
        ]);

        $Rent_logs = Rent_logs::findOrFail($id);
        $Rent_logs->update($request->all());

        return new RentdetailResource($Rent_logs->loadMissing('user:id,username,nope,address', 'book:id,book_code,title'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $Rent_logs = Rent_logs::findOrFail($id);
        $Rent_logs->delete();

        return new RentdetailResource($Rent_logs);
    }
}
