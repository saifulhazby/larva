<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserDetailResource;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = User::get();
        //return response()->json(['data' => $data]);
        return UserResource::collection($user);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required|min:6|max:50',
            'jeniskelamin' => 'required',
            'address' => 'required',
            'nope' => 'required',
            // 'role_id' => 'required|exists:rolea,id',
        ]);

        //$request['user'] = Auth::user()->id;
        $user = User::create($request->all());
        return new UserDetailResource($user->loadMissing('role:id,name'));
        //return response()->json('wogeh');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = User::with('role:id,name')->FindOrFail($id);
        //return response()->json(['data' => $data]);
        return new UserDetailResource($user);

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            
        ]);

        $user = User::findOrFail($id);
        $user->update($request->all());

        return new UserdetailResource($user);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return new UserdetailResource($user);
    }
}
