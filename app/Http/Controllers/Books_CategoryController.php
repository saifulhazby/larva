<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\BookCategoryResource;
use App\Http\Resources\BookCategoryDetailResource;
use App\Models\Books_category;


class Books_CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $book_category = Books_category::all();
        //return response()->json(['data' => $book_category]);
        return BookCategorydetailResource::collection($book_category->loadMissing(['book:id,book_code,title,status', 'category:id,name']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'book_id' => 'required|exists:books,id',
            'category_id' => 'required|exists:categories,id',
        ]);

        $book_category = Books_category::create($request->all());
        return new BookCategorydetailResource($book_category);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $book_category = Books_category::with('book:id,book_code,title,status', 'category:id,name')->FindOrFail($id);
        //return response()->json(['data' => $book_category]);
        return new BookCategoryDetailResource($book_category->loadMissing('book:id,book_code,title,status', 'category:id,name'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'book_id' => 'required|exists:books,id',
            'category_id' => 'required|exists:categories,id',
        ]);

        $book_category = Books_category::findOrFail($id);
        $book_category->update($request->all());

        return new BookCategorydetailResource($book_category->loadMissing('book:id,book_code,title,status', 'category:id,name'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $book_category = Books_category::findOrFail($id);
        $book_category->delete();

        return new BookCategorydetailResource($book_category);
    }
}
