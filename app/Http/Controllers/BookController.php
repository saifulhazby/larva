<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Http\Resources\BookResource;
use App\Http\Resources\BookDetailResource;
use Illuminate\Http\Request;

use function Pest\Laravel\json;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $book = Book::get();
        //return response()->json(['data' => $book]);
        return BookResource::collection($book);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'book_code' => 'required',
            'title' => 'required',
            'status' => 'required',
        ]);

        //$request['user'] = Auth::user()->id;
        $book = Book::create($request->all());
        return new BookDetailResource($book);
        //return response()->json('wogeh');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $book = book::FindOrFail($id);
        //return response()->json(['data' => $data]);
        return new BookDetailResource($book);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'book_code' => 'required',
            'title' => 'required',
            'status' => 'required',
        ]);

        $book = Book::findOrFail($id);
        $book->update($request->all());

        return new BookdetailResource($book);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $book = Book::findOrFail($id);
        $book->delete();

        return new BookdetailResource($book);
    }
}
