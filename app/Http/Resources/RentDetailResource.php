<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RentDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'Book_id' => $this->book_id,
            'User_id' => $this->user_id,
            'rent_date' => $this->rent_date,
            'return_date' => $this->return_date,
            'actual_return_date' => $this->actual_return_date,
            'peminjam' => $this->whenLoaded('user'),
            'buku yang dipinjam' => $this->whenLoaded('book'),
            // 'peminjam' => $this->whenLoaded('peminjam'),
            'created_at' => date_format($this->created_at,"d-m-Y H:i:s"),
        ];
    }
}
