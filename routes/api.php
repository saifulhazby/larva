<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\RentController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\Books_CategoryController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\AuthenController;
use Illuminate\Http\Request;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    //     return $request->user();
// });

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('user', [UserController::class, 'index']);
    Route::get('user/{id}', [UserController::class, 'show']);
    Route::patch('user/{id}', [UserController::class, 'update']);
    Route::delete('user/{id}', [UserController::class, 'destroy'])->middleware('Pemilik');
    
    Route::get('rent', [RentController::class, 'index']);
    Route::patch('rent/{id}', [RentController::class, 'update'])->middleware('Pemilik');
    Route::get('rent/{id}', [RentController::class, 'show']);
    Route::post('rent', [RentController::class, 'store']);
    //Route::post('rent{book_id}', [RentController::class, 'store']);
    Route::delete('rent/{id}', [RentController::class, 'destroy'])->middleware('Pemilik');
    
    Route::post('book', [BookController::class, 'store']);
    Route::patch('book/{id}', [BookController::class, 'update']);
    Route::delete('book/{id}', [BookController::class, 'destroy']);
    
    Route::post('Books_category', [Books_CategoryController::class, 'store']);
    Route::patch('Books_category/{id}', [Books_CategoryController::class, 'update']);
    Route::delete('Books_category/{id}', [Books_CategoryController::class, 'delete']);
    
    Route::post('category', [CategoryController::class, 'store']);
    Route::patch('category/{id}', [CategoryController::class, 'update']);
    Route::delete('category/{id}', [CategoryController::class, 'destroy']);
    
    Route::get('logout', [AuthenController::class, 'logout']);
    Route::get('me', [AuthenController::class, 'me']);
});



Route::get('role', [RoleController::class, 'index']);
Route::get('role/{id}', [RoleController::class, 'show']);

Route::post('user', [UserController::class, 'store']);

Route::get('book', [BookController::class, 'index']);
Route::get('book/{id}', [BookController::class, 'show']);

Route::get('Books_category/{id}', [Books_CategoryController::class, 'show']);
Route::get('Books_category', [Books_CategoryController::class, 'index']);

Route::get('category/{id}', [CategoryController::class, 'show']);
Route::get('category', [CategoryController::class, 'index']);

Route::post('login', [AuthenController::class, 'login']);
