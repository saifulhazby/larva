<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use App\Models\Book;
use App\Models\Books_Category;
use App\Models\Role;
use App\Models\category;
use App\Models\Rent_logs;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        Role::create([
            'name' => 'user',
        ]);
        
        Role::create([
            'name' => 'admin',
        ]);


        User::create([
            'username' => 'kambing',
            'password' => '$2y$10$aV8mF0yxZGCNg33vYBy4K.eZpXrvnSuHjN6c1KO/k7KsL4uk7vvUG',
            'role_id' => '2',
            'nope' => '087755228507',
            'jeniskelamin' => 'pria',
            'address' => 'sampang',
            'status' => 'meminjam',
        ]);

        User::create([
            'username' => 'ayam',
            'password' => '123123',
            'role_id' => '1',
            'jeniskelamin' => 'pria',
            'nope' => '087755228507',
            'address' => 'sampang',
            'status' => 'admin',
        ]);

        User::create([
            'username' => 'kucing',
            'password' => '123412',
            'role_id' => '1',
            'jeniskelamin' => 'wanita',
            'nope' => '087755228507',
            'address' => 'sampang',
            'status' => 'meminjam',
        ]);

        Book::create([
            'book_code' => '111',
            'title' => 'tahu_bulat',
            'status' => 'soldout',
        ]);

        Book::create([
            'book_code' => '123',
            'title' => 'tahu_segitiga',
            'status' => 'ready',
        ]);

        Book::create([
            'book_code' => '121',
            'title' => 'tahu_kotak',
            'status' => 'ready',
        ]);

        Rent_logs::create([
            'user_id' => '1',
            'book_id' => '1',
            'rent_date' => '2023-05-10',
            'return_date' => '2023-05-15',
            'actual_return_date' => '2023-05-15',
        ]);

        Rent_logs::create([
            'user_id' => '2',
            'book_id' => '2',
            'rent_date' => '2023-05-15',
            'return_date' => '2023-05-17',
            'actual_return_date' => '2023-05-19',
        ]);

        Rent_logs::create([
            'user_id' => '2',
            'book_id' => '3',
            'rent_date' => '2023-05-15',
            'return_date' => '2023-05-17',
            'actual_return_date' => '2023-05-19',
        ]);

        
        category::create([
            'name' => 'Comedy',
        ]);
        
        category::create([
            'name' => 'Horor',
        ]);
        Books_Category::create([
            'book_id' => '1' ,
            'category_id' => '1',
        ]);

        Books_Category::create([
            'book_id' => '2' ,
            'category_id' => '2',
        ]);

        Books_Category::create([
            'book_id' => '3' ,
            'category_id' => '1',
        ]);
    }
}
